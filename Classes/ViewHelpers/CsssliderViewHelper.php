<?php

namespace HIVE\HiveCptCntCssslider\ViewHelpers;

/***
 *
 * This file is part of the "hive_cpt_cnt_cssslider" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Class CsssliderViewHelper
 * @package HIVE\HiveCptCntCssslider\ViewHelpers
 */
class CsssliderViewHelper extends AbstractTagBasedViewHelper
{

    /**
     * csssliderService
     *
     * @var \HIVE\HiveCptCntCssslider\Service\CsssliderService
     * @inject
     */
    protected $csssliderService = null;

    /**
     * use:
     * <div xmlns="http://www.w3.org/1999/xhtml" lang="en"
     *      xmlns:f="http://typo3.org/ns/TYPO3/Fluid/ViewHelpers"
     *      xmlns:hiveCptCntCssslider="http://typo3.org/ns/HIVE/HiveCptCntCssslider/ViewHelpers">
     *
     * ...
     *
     * <hiveCptCntCssslider:cssslider aSlides="{0: '1', 1: '2'}" sUniqueIdentifier="uniqueIdentifier" />
     * <hiveCptCntCssslider:cssslider aSlides="{0: '1', 1: '2'}" sUniqueIdentifier="uniqueIdentifier" aClasses="{0: '1', 1: '1', 2: '1', 3: '1', 4: '1'}"/>
     *
     *
     * @param array $aSlides
     * @param string $sUniqueIdentifier
     * @param array $aClasses
     *
     * @return string
     */
    public function render(
        array $aSlides = [],
        string $sUniqueIdentifier = "",
        array $aClasses = [1, 1, 1, 1, 1]
    ): string {

        if ($aClasses[0] < 1 or $aClasses[0] > 1) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }
        if ($aClasses[1] < 1 or $aClasses[1] > 1) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }
        if ($aClasses[2] < 1 or $aClasses[2] > 2) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }
        if ($aClasses[3] < 1 or $aClasses[3] > 3) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }
        if ($aClasses[4] < 1 or $aClasses[4] > 5) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }

        $iCount = count($aSlides);

        if ($iCount < 1) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }

        $sSlides = "";
        for ($i = 1; $i <= $iCount; $i++) {
            $h = $i - 1;

            $sSlides .=
                "<div class='csss__slide col'>" .
                $aSlides[$h] .
                "</div>";
        }

        $sHtml = $this->csssliderService->createSliderHtml($iCount, $sUniqueIdentifier, $aClasses);
        $sHtml = '<div class="tx-hive-cpt-cnt-cssslider">' . str_replace("###SLIDES###", $sSlides, $sHtml) . '</div>';

        return $sHtml;

    }


}
