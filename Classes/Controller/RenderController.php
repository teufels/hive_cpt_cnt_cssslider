<?php

namespace HIVE\HiveCptCntCssslider\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_cssslider" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

/**
 * RenderController
 */
class RenderController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * renderRepository
     *
     * @var \HIVE\HiveCptCntCssslider\Domain\Repository\RenderRepository
     * @inject
     */
    protected $renderRepository = null;

    /**
     * csssliderService
     *
     * @var \HIVE\HiveCptCntCssslider\Service\CsssliderService
     * @inject
     */
    protected $csssliderService = null;

    /**
     * action oneOneTwoThreeFour
     *
     * @return void
     */
    public function oneOneTwoThreeFourAction()
    {
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];
        $aSettings = $this->settings;
        $aSlidePageUids = $this->getPagesForGivenParentPageUid($aSettings["cssslider"]["parentPageUid"]);

        $sHtml = $this->csssliderService->createSliderHtml(count($aSlidePageUids), $iPluginUid, [1, 1, 2, 3, 4]);
        $aHtml = explode('###SLIDES###', $sHtml);
        $this->view->assign('aHtml', $aHtml);
        $this->view->assign('aSlidePageUids', $aSlidePageUids);
    }

    /**
     * action oneOneOneOneOne
     *
     * @return void
     */
    public function oneOneOneOneOneAction()
    {
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];
        $aSettings = $this->settings;
        $aSlidePages = $this->getPagesForGivenParentPageUid($aSettings["cssslider"]["parentPageUid"], false);

        $sHtml = $this->csssliderService->createSliderHtml(count($aSlidePages), $iPluginUid, [1, 1, 1, 1, 1]);
        $aHtml = explode('###SLIDES###', $sHtml);
        $this->view->assign('aHtml', $aHtml);
        $this->view->assign('aSlidePages', $aSlidePages);

    }

    /**
     * @param int $iParentPageUid
     * @param bool $aReturnUidsOnly
     *
     * @return array
     */
    protected function getPagesForGivenParentPageUid(int $iParentPageUid, bool $aReturnUidsOnly = true): array
    {
        // get data
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $queryBuilder
            ->select(
                'pages.uid AS uid',
                'pages.doktype AS doktype'
            )
            ->from('pages')
            ->where('pages.pid IN (' . $iParentPageUid . ')')
            ->andWhere('pages.hidden = 0')
            ->andWhere('pages.deleted = 0');

        $aResult = $queryBuilder->execute()->fetchAll();
        if ( ! $aReturnUidsOnly) {
            return $aResult;
        }
        $aReturn = [];
        foreach ($aResult as $aUid) {
            $aReturn[] = $aUid["uid"];
        }

        return $aReturn;
    }
}
