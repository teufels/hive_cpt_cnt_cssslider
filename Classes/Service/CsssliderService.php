<?php

namespace HIVE\HiveCptCntCssslider\Service;

/***
 *
 * This file is part of the "hive_cpt_cnt_cssslider" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * Class CsssliderService
 * @package HIVE\HiveCptCntCssslider\Service
 */
class CsssliderService implements \TYPO3\CMS\Core\SingletonInterface
{

    /**
     * @param int $iSlides
     * @param string $sUniqueIdentifier
     * @param array $aClasses
     *
     * @return string
     */
    public function createSliderHtml(
        int $iSlides = 0,
        string $sUniqueIdentifier = "",
        array $aClasses = [1, 1, 1, 1, 1]
    ): string {

        if ($aClasses[0] < 1 or $aClasses[0] > 1) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }
        if ($aClasses[1] < 1 or $aClasses[1] > 1) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }
        if ($aClasses[2] < 1 or $aClasses[2] > 2) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }
        if ($aClasses[3] < 1 or $aClasses[3] > 3) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }
        if ($aClasses[4] < 1 or $aClasses[4] > 5) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }

        $iCount = $iSlides;

        if ($iCount < 1) {
            return "Error " . __CLASS__ . " " . __FUNCTION__ . "[" . __LINE__ . "]";
        }

        foreach ($aClasses as $iK => $iV) {
            if ($iCount < $iV) {
                $aClasses[$iK] = $iCount;
            }
        }

        $sInputs = "";
        $sDots = "";

        $sArrowsPrev = "";

        $sArrowsNext = "";
        $sSlides = "";
        for ($i = 1; $i <= $iCount; $i++) {
            $h = $i - 1;
            $j = $i + 1;
            $sInputs .= "<input " . ($i == 1 ? 'checked' : '') . " id='slide__$sUniqueIdentifier--$i' name='s--$sUniqueIdentifier' type='radio' class='csss_input'>";
            $sDots .= "<label class='dot btn btn-secondary' for='slide__$sUniqueIdentifier--$i'>$i</label>";

            /*
             * XS and SM
             */
            if ($i == 1) {
                $sArrowsPrev .= "<label class='prev ' for='slide__$sUniqueIdentifier--$iCount'>&laquo;</label>";
            } else {
                $sArrowsPrev .= "<label class='prev' for='slide__$sUniqueIdentifier--$h'>&laquo;</label>";
            }
            if ($i == $iCount) {
                $sArrowsNext .= "<label class='next' for='slide__$sUniqueIdentifier--1'>&raquo;</label>";
            } else {
                $sArrowsNext .= "<label class='next' for='slide__$sUniqueIdentifier--$j'>&raquo;</label>";
            }

        }

        $sHtml =
            "<div class='csss--vh iXs--$aClasses[0] iSm--$aClasses[1] iMd--$aClasses[2] iLg--$aClasses[3] iXl--$aClasses[4] iCount--$iCount' id='csss--$sUniqueIdentifier'>" .
            $sInputs .
            "<nav class='dots btn-group'>" .
            "<div class='prev btn btn-secondary'>" . $sArrowsPrev . "</div>" . $sDots . "<div class='next btn btn-secondary'>" . $sArrowsNext . "</div>" .
            "</nav>" .
            "<div class=\"csss__wrapper\">" .
            "<div class=\"csss__slider row d-flex iXs--$aClasses[0] iSm--$aClasses[1] iMd--$aClasses[2] iLg--$aClasses[3] iXl--$aClasses[4] iCount--$iCount\">" .
            "###SLIDES###" .
            "</div>" .
            "</div>" .
            "</div>";

        return $sHtml;

    }


}
